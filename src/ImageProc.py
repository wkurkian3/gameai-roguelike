import Properties
import pygame
import Physics
import os

tilesDir = "images/"
spritesDir ="images/"
nullcolor = [0,0,0]
animationDir = "images/"
iconDir = "images/"

def getUI():
    image = pygame.image.load(os.path.join("ui","ui.png")).convert()
    image.set_colorkey(image.get_at((int(image.get_width()/2),int(image.get_height()/2))))
    properties = Properties.makeProperties("ui",Properties.UI)
    return {"image" :image, "properties" : properties}

def getSurface(color,size):
    image = pygame.Surface(size)
    image.fill(color)
    return image

def getIcon(iconName):
    image = pygame.image.load(os.path.join(iconDir,iconName+'.png')).convert()
    image.set_colorkey(image.get_at([0,0]))
    return image

def getAnimation(animationName):
    properties = Properties.makeProperties(animationName, Properties.ANIMATION)
    animation = pygame.image.load(animationDir + animationName+".png").convert()
    size = animation.get_size()
    effect = properties.getBranch("animation")
    sequ = effect.getArray("sequence")
    dims = effect.getArray("dimensions")
    x = sequ[0]%dims[0] * sequ[2]
    y = sequ[0]//dims[0] * sequ[3]
    count = sequ[0]
    images = []
    while count < sequ[1]:
        imgsize = [sequ[2],sequ[3]]
        image = getSurface(nullcolor,[imgsize[0],imgsize[1]])
        image.blit(animation,[0,0],[x,y,imgsize[0],imgsize[1]])
        image.set_colorkey(image.get_at([0,0]))
        images.append(image)
        count+=1
        x += sequ[2]
        if (x >= size[0]):
            x = 0
            y+= sequ[3]
            
    #pygame.image.save(animation,'test/'+animationName+'test.png')
    data = {}
    data["props"] = effect
    data["images"] = images
    data["size"] = imgsize
    return data
  
def getTileset(tilesetname):
    properties = Properties.makeProperties(tilesetname, Properties.TILE)
    tileset = pygame.image.load(tilesDir + tilesetname+".png").convert()
    size = [120,86]
    x = 1
    y = 1
    shift = 17
    i = 0
    tiledict = {}
    while y < size[1]:
        imgsize = (shift-1)*2
        image = getSurface(nullcolor,[imgsize,imgsize])
        image.blit(tileset,[0,0],[x,y,shift-1,shift-1])
        image.blit(tileset,[imgsize/2,0],[x,y,shift-1,shift-1])
        image.blit(image,[0,imgsize/2],[0,0,imgsize,imgsize/2])
        if (x >= size[0]):
            x = 1
            y+= shift
        else:
            x += shift
        tiledict[str(i)] = image
        if str(i) in properties.props.keys():
            tiledict[image] = properties.getBranch(str(i))
        else:
            tiledict[image] = Properties.Properties.getDefaultProperties(Properties.defaultTileProps)
        if 'escape' in tiledict[image].getType():
            tiledict['>'] = image

        i+=1
    image = getSurface(nullcolor,[imgsize,imgsize])
    tiledict["-"] = image
    tiledict[image] = Properties.Properties.getDefaultProperties(Properties.defaultTileProps)
    tiledict["size"] = [imgsize,imgsize]
    return tiledict

def getSprite(spritename,dimensions):
    properties = Properties.makeProperties(spritename, Properties.SPRITE)
    spritesheet = pygame.image.load(spritesDir + spritename+".png").convert()
    size = spritesheet.get_size()
    color = spritesheet.get_at((0,0))
    x = 0
    y = 0
    shift = [size[0]/dimensions[0],(size[1])/dimensions[1]]
    i = 0
    sprites = {"images" : {}}
    while y < size[1]:
        
        image = getSurface(color,shift)
        image.blit(spritesheet,[0,0],[x,y,shift[0],shift[1]])
        image.set_colorkey(image.get_at((0,0)))
        
        
        if i in sprites["images"]:
            sprites["images"][i].append(image)
        else:
            sprites["images"][i] = [image]
        x += shift[0]
        if (x >= size[0]):
            x = 0
            y+= shift[1]+1
            i+=1
    sprites["properties"] = properties
    return sprites

#index of sprite
def getIndexFromDir(sprites,direction):
    
    a = Physics.directionToCardinalNumber(direction)
    return a


def getEntry(dictionary,index):
    return dictionary[str(index)]
