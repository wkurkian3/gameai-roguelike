import pygame

def getSurface(color,size):
    image = pygame.Surface(size)
    image.fill(color)
    return image
