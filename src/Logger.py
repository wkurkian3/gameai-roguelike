from Statistics import Statistics
import item.Item as Item
import Physics

instance = None


def init():
    global instance
    instance = Statistics()

def addItemCount(type):
    global instance
    if type == Item.Item.MELEE:
        instance.numMeleeItems += 1
    elif type == Item.Item.RANGED:
        instance.numRangedItems += 1
    elif type == Item.Item.MAGIC:
        instance.numMagicItems += 1
    elif type == Item.Item.OTHER:
        instance.numOtherItems += 1
    instance.totalItems += 1

def addItemUse(type):
    global instance
    if type == Item.Item.MELEE:
        instance.meleeUses += 1
    elif type == Item.Item.RANGED:
        instance.rangedUses += 1
    elif type == Item.Item.MAGIC:
        instance.magicUses += 1
    elif type == Item.Item.OTHER:
        instance.otherUses += 1
    instance.totalUses += 1

def enemyKilled():
    global instance
    instance.enemiesKilled += 1

def damageTaken(damage):
    global instance
    instance.damageTaken += damage

def setElapsedTime(time):
    global instance
    instance.timeToComplete = time

def totalEnemies(count):
    global instance
    instance.totalEnemies = count

def distanceToGoal(start,goal):
    global instance
    instance.startToGoalDist = ((float(start[0]-goal[0]))**2 + (float(start[1]-goal[1]))**2)**0.5
    
def display():
    global instance
    instance.displayStats()

import Physics

instance = None


def init():
    global instance
    instance = Statistics()

def addItemUse(type):
    global instance
    if type == Item.Item.MELEE:
        instance.meleeUses += 1
    elif type == Item.Item.RANGED:
        instance.rangedUses += 1
    elif type == Item.Item.MAGIC:
        instance.magicUses += 1
    elif type == Item.Item.OTHER:
        instance.otherUses += 1
    instance.totalUses += 1

def enemyKilled():
    global instance
    instance.enemiesKilled += 1

def damageTaken(damage):
    global instance
    instance.damageTaken += damage

def setElapsedTime(time):
    global instance
    instance.timeToComplete = time

def totalEnemies(count):
    global instance
    instance.totalEnemies = count

def distanceToGoal(start,goal):
    global instance
    instance.startToGoalDist = ((float(start[0]-goal[0]))**2 + (float(start[1]-goal[1]))**2)**0.5
    
def display():
    global instance
    instance.displayStats()
