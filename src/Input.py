import pygame
import Physics
arrowkeys = [0,0,0,0]
quit = False
mousePressed  = False
keyReleased = False
def checkInput():
    global quit
    global arrowkeys
    global keyReleased
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit = True
        if event.type == pygame.KEYUP:
            keyReleased = True
        else:
            keyReleased = False
    keys = pygame.key.get_pressed()
    
    if keys[pygame.K_q]:
        quit = True
    
    arrowkeys = Physics.setVectorValue(arrowkeys,0)
    if keys[pygame.K_UP]:
        arrowkeys[0] = 1
    if keys[pygame.K_RIGHT]:
        arrowkeys[1] = 1
    if keys[pygame.K_DOWN]:
        arrowkeys[2] = 1
    if keys[pygame.K_LEFT]:
        arrowkeys[3] = 1
    
def moveArrowKeys(pos,step=5):
    vel = [0,0]
    vel[0] = arrowkeys[1]-arrowkeys[3]
    vel[1] = arrowkeys[2]-arrowkeys[0]
    vel = Physics.vectorByScalar(vel,step)
    pos = Physics.vectorAdd(pos,vel)

    return pos

def checkDoMainAction():
    if keys[pygame.K_Z]:
        return True
    return False

def quitPressed():
    return quit

def getKeys():
    return pygame.key.get_pressed()

def getMouseState(offset,afterPress = False):
    global mousePressed
    state = pygame.mouse.get_pressed()
    if not state[0] or afterPress:
        mousePressed = False
    if not state[0] or mousePressed:
        
        return [False,[int(i) for i in Physics.vectorSub(pygame.mouse.get_pos(),offset)]]
    else:
        mousePressed = True
        return [True,[int(i) for i in Physics.vectorSub(pygame.mouse.get_pos(),offset)]]
        
def waitForInput():
    return keyReleased
