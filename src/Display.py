import pygame
import Physics
import Sprites
import Input
import Camera

black    = (   0,   0,   0)
white    = ( 255, 255, 255)
green    = (   0, 255,   0)
red      = ( 255,   0,   0)
blue     = (   0,   0, 255)
size=[400,400]

ui = None
uiPlayer = None
spellIndices = []
animationTimer = pygame.time.get_ticks()
currentArea = None
tiles = pygame.sprite.LayeredUpdates()
overlay = pygame.sprite.LayeredUpdates()
objects = []
screen = None
clock = None
background = pygame.Surface(size)
background.fill(black)
viewpoint = None
mouseClick = [False,None]
dots = []
mouseRect = [0,0]
lastMouse = [0,0]
gridY = 0
tileFlag = None
offset = [0,0]
center = [0,0]
camera = None

def generateOverlay(sprite):
    if isinstance(sprite,Sprites.Tile):
        height = sprite.props.getHeight()
        height -= 1
        if (height <= 0):
            return [sprite]
        dims = list(sprite.rect.size)
        dims[1] = height * dims[1]
        image = sprite.image.subsurface(pygame.Rect((0,0,dims[0],dims[1])))
        location = sprite.rect.topleft
        tileUp = Sprites.Tile([location[0],location[1]-height*dims[1]],image,sprite.props,sprite._layer)
        tileLeft = Sprites.Tile([location[0]-height*dims[0],location[1]],image,sprite.props,sprite._layer)
        return [tileUp]
    else:
        return [sprite]
def init(psize = [400,400]):
    global clock
    global screen
    global size
    global gridY
    global offset
    global viewpoint
    pygame.init()
    size = psize
    screen=pygame.display.set_mode(size,pygame.DOUBLEBUF)
    screen.blit(background,[0,0])
    pygame.display.set_caption("Project 4")
    clock=pygame.time.Clock() 

def setUI(uiIn):
    global ui
    ui = uiIn
def stop():
    pygame.quit()

def flip():
    pygame.display.flip()

def wait(millis):
    global clock
    clock.tick(millis)

def showStart():
    global screen
    screen.fill(black)
    color = [255,255,255]
    font = pygame.font.Font(None, 20)
    text = font.render(' Team Rogue\'s Roguelike', 1, color)
    textpos = text.get_rect()
    textpos.topleft = screen.get_rect().topleft
    screen.blit(text, textpos)
    
    text2 = font.render(' Press enter to start playing. ',1,color)
    textpos2 = text2.get_rect()
    textpos2.topleft = textpos.bottomleft
    screen.blit(text2,textpos2)
    
    text3 = font.render(' How to play: ',1,color)
    textpos3 = text3.get_rect()
    textpos3.topleft = textpos2.bottomleft
    screen.blit(text3,textpos3)
    
    text4 = font.render(' Use the arrow keys to move. Press a to pickup items. Press z to use combat items. Press x to use utility items. ',1,color)
    textpos4 = text4.get_rect()
    textpos4.topleft = textpos3.bottomleft
    screen.blit(text4,textpos4)

    text5 = font.render(' Try to get as far as you can. Press space to go the next level when standing on a strange looking tile.',1,color)
    textpos5 = text5.get_rect()
    textpos5.topleft = textpos4.bottomleft
    screen.blit(text5,textpos5)
    

    flip()

def redraw(level):
    global screen
    global tiles
    global gridY
    global currentArea
    global uiPlayer
    global spellIndices
    global animationTimer
    global lastUpdatePos
    if currentArea.update:
        currentArea.update = False
        setArea(currentArea)
    camera.update(viewpoint)
    screen.fill(black)
    mid = Physics.vectorByScalar(size, 0.5)
    offset = Physics.vectorSub(mid, viewpoint.rect.topleft)
    for sprite in tiles.sprites():
        if sprite.layerChanged == 1:
            sprite.layerChanged = 0
            tiles.change_layer(sprite, sprite._layer)
        sprite.update(pygame.time.get_ticks(),currentArea)
        screen.blit(sprite.image,camera.apply(sprite))
    #tilerectlist = tiles.draw(screen)#pygame.display.update(tilerectlist+charrectlist)
    if True:
        color = [255,255,255]
        font = pygame.font.Font(None, 36)
	text = font.render(' Level: '+str(level)+' Health: ' + str(viewpoint.character.health)+ ' Attack: ' + str(viewpoint.character.attack) + ' Speed: '+ str(viewpoint.character.speed), 1, color)
        textpos = text.get_rect()
	textpos.bottomleft = screen.get_rect().bottomleft
	screen.blit(text, textpos)
        text2 = font.render(' Item: ',1,color)
        textpos2 = text2.get_rect()
        textpos2.bottomleft = textpos.topleft
        screen.blit(text2,textpos2)
        textSide = font.render(' Side Item: ',1,color)
        textposSide = textSide.get_rect()
        textposSide.bottomleft = textpos2.topleft
        screen.blit(textSide,textposSide)
        
        if not viewpoint.item == None:
            icon = viewpoint.item.getIcon()
            rect = icon.get_rect()
            loc = [textpos2.topright[0],textpos.topleft[1]-rect.height]
            screen.blit(icon,loc)
            text3 = font.render(' Attack: ' +str(viewpoint.item.attack) +' Speed ' + str(viewpoint.item.speed),1,color)
            textpos3 = text3.get_rect()
            textpos3.topleft = [loc[0]+rect.width,textpos2.topright[1]]
            screen.blit(text3,textpos3)
        if not viewpoint.sideItem == None:
            icon = viewpoint.sideItem.getIcon()
            rect = icon.get_rect()
            loc = [textposSide.topright[0],textposSide.topright[1]]
            screen.blit(icon,loc)
            text6 = font.render(' Defense ' +str(viewpoint.sideItem.defense) +' Speed ' + str(viewpoint.sideItem.speed),1,color)
            textpos6 = text6.get_rect()
            textpos6.topleft = [loc[0]+rect.width,textposSide.topright[1]]
            screen.blit(text6,textpos6)
    for actor in currentArea.actors:
        if actor.itemActive and not actor.itemFrame[1] == None and len(actor.itemFrame[1]) > 0:
            for i in range(len(actor.itemFrame[1])):
                screen.blit(actor.itemFrame[1][i],camera.applyCoords(actor.itemFrame[0][i]))
        if actor.sideItemActive and not actor.sideItemFrame[1] == None and len(actor.sideItemFrame[1]) > 0:
            for i in range(len(actor.sideItemFrame[1])):
                screen.blit(actor.sideItemFrame[1][i],camera.applyCoords(actor.sideItemFrame[0][i]))

#user interface display code: for now just display spells in bar next to M
    #screen.blit(ui.image["image"],[0,0])
    for dot in dots:
         pygame.draw.circle(screen,blue,[int(i) for i in Physics.vectorAdd(dot, offset)],5)
 ##  if not currentArea == None:
 ##        for x in range(len(currentArea.tiles)):
 ##            pygame.draw.line(screen,white,Physics.vectorAdd(offset,[0,x*currentArea.tilesize[0]]),Physics.vectorAdd(offset,[gridY*currentArea.tilesize[1], x*currentArea.tilesize[0]]))
 ##        for x in range(gridY):
 ##            pygame.draw.line(screen,white,Physics.vectorAdd(offset,[x*currentArea.tilesize[0],0]),Physics.vectorAdd(offset,[x*currentArea.tilesize[0],len(currentArea.tiles)*currentArea.tilesize[1]]))

    if tileFlag != None:
         pos = currentArea.getTilePosition(tileFlag)
         pos = [int(i) for i in pos]
         pygame.draw.circle(screen,green,[int(i) for i in Physics.vectorAdd(pos, offset)],5)

    flip()

def addObject(obj):
     global objects
     objects.append(obj)
def getOffset():
     global offset
     return offset
def setArea(area):
     global currentArea
     global tiles
     global viewpoint
     global gridY
     global camera
     currentArea = area
     maxlen = 0
     for x in currentArea.tiles:
         maxlen = max(maxlen,len(x))
     gridY = maxlen
     viewpoint = currentArea.getPlayer()
     #viewpoint = currentArea.tiles[int(len(currentArea.tiles)/2)][int(gridY/2)]
     tiles.empty()
     for tilerow in currentArea.tiles:
         for tile in tilerow:
             if not tile == None:
                 tiles.add(tile)
     n = 0
     for sprite in area.actors:
         tiles.add(sprite)
     for item in area.items:
         tiles.add(item)
     offset = Physics.vectorSub(center, viewpoint.rect.topleft)
     for i in tiles.sprites():
         tile = generateOverlay(i)
         n+= 1
         for t in tile:
             tiles.add(t)
     setPlayer(currentArea.getPlayer())
     maxRow = -1
     maxRowValue = -1
     for i in range(len(area.tiles)):
         if (len(area.tiles[i]) > maxRowValue):
             maxRow = i
             maxRowValue = len(area.tiles[i])
     
     height = len(area.tiles)*area.tilesize[1]
     width = maxRowValue*area.tilesize[0]
     camera = Camera.Camera(Camera.complex_camera,width,height,size)

def setPlayer(player):
    global uiPlayer
    global spellIndices
    uiPlayer = player
    viewpoint = player
    spells = uiPlayer.character.spells
    spellIndices = [] 
    for i in spells:
        spellIndices.append(0)

def setMouse(mouse):
    global mouseClick
    global mouseRect
    global lastMouse
    if mouse[1] == lastMouse:
        return False
    mouseClick[0] = mouse[0]
    mouseClick[1] = mouse[1]
    if not mouseClick[0]:
        mouseRect = [0,0]
    else:
        
        if lastMouse == [0,0]:
            lastMouse= mouse[1]
        else:
            Physics.vectorAdd(mouseRect, Physics.vectorSub(mouse[1],lastMouse))
            lastMouse = mouse[1]
        #print (lastMouse,lastMouse == [0,0],mouseRect)
def setDots(dotList):
    global dots
    dots = dotList
def flagTile(tilePosition):
    global tileFlag
    tileFlag = tilePosition
def getJob():
    return {"player": currentArea.getPlayer(),"area" : currentArea.clone()}
def updateOffset(keys):
    global offset
    global center
    center = Input.moveArrowKeys(center)
    mid = Physics.vectorByScalar(size, 0.5)
    offset = Physics.vectorSub(mid, center)
