from level.agent.agent import Agent
import random

class SmoothingAgent(Agent):

    def execute(self):
        i = 50
        while (i > 0):
            x = random.randint(2, self.width-2)
            y = random.randint(2, self.height-2)
            self.area[y][x] = self.smooth(x, y)
            i -= 1

    def smooth(self, x, y):
        count = [0, 0]
        for i in range(y-1, y+2):
            for j in range(x-1, x+2):
                if (j >= 0 and i >= 0 and j < self.width and i < self.height):
                    if (self.area[i][j] == 1):
                        count[0] += 1
                    if (self.area[i][j] == 3):
                        count[1] += 1
        return 1 if count[0] > count[1] else 3
