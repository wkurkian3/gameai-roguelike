from level.agent.agent import Agent
import Sprites
import Character
import random

class EnemyAgent(Agent):

    def __init__(self, area, width, height, player_model, current_level, actors, size):
        super(EnemyAgent, self).__init__(area, width, height, player_model)
        self.current_level = current_level
        self.actors = actors
        self.size = size

    def execute(self):
        i = 5 + self.current_level
        while (i > 0):
            x = random.randint(2, self.width-2)
            y = random.randint(2, self.height-2)
            if (self.area[y][x] == 3):

                itemAttack = random.randint(self.current_level-2, self.current_level)
                itemSpeed = random.randint(self.current_level-2, self.current_level)
                if (itemAttack < 1): itemAttack = 1
                if (itemSpeed < 1): itemSpeed = 1

                character = Character.Character()
                character.attack = random.randint(self.current_level-2, self.current_level)
                if (character.attack < 1): character.attack = 1
                character.speed = random.randint(self.current_level-2, self.current_level+2)
                if (character.speed < 1): character.speed = 1

                threshold = 0.5 + self.player_model.meleePref - self.player_model.rangedPref
                if (threshold < 0.2): threshold = 0.2
                if (threshold > 0.8): threshold = 0.8

                if (random.random() < threshold):
                    character.health = random.randint(self.current_level, self.current_level+3)
                    enemy = Sprites.buildWarrior(character, itemAttack, itemSpeed)
                    enemy.rect.topleft = [self.size[0] * x, self.size[1] * y]
                    self.actors.append(enemy)
                else:
                    character.health = random.randint(self.current_level - 2, self.current_level)
                    if (character.health < 1): character.health = 1
                    enemy = Sprites.buildArcher(character, itemAttack, itemSpeed)
                    enemy.rect.topleft = [self.size[0] * x, self.size[1] * y]
                    self.actors.append(enemy)
                i -= 1
