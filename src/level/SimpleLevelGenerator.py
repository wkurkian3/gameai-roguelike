import Area
import LevelGenerator
import ImageProc
import Sprites
import Character
import statemachine.MeleePursuit
import item.Item as Item

class SimpleLevelGenerator(LevelGenerator.LevelGenerator):
    def __init__(self):
        x = 0
    def createLevel(self,player,playerModel,currentLevel):
        #Area.buildMap might be helpful to look at for this.
        #It builds a map from the textfiles in the map folder.
        
        #this indicates which set of tiles you are using. Currently only walls exists.
        #Look at image/walls.txt and images/wall.png for more information.
        tiledict = ImageProc.getTileset('walls')
        connections = {}# leave empty for now. This feature needs work
        size = tiledict['size'] #size of the tiles. This comes from a tileset
        start = [2*size[0],2*size[1]]
        #this is a row by column tile map where you fill in the type of tiles
        #note that it must be rectangular, so empty spaces must have blank spaces
        #get blank by using a blank image - get it by: image = tiledict['-']
        area = [[1,1,1,1,1,1],
                [1,3,0,3,'>',1],
                [1,3,3,3,3,1],
                [1,3,3,3,3,1],
                [1,1,1,1,1,1]]
        
        for y in range(len(area)):
            for x in range(len(area[y])):
                pos = [x*size[0],y*size[1]]
                image = tiledict[str(area[y][x])]
                props = tiledict[image]
                layer = Area.detLayer(pos,size,props)
                area[y][x] = Sprites.Tile(pos,image,props,layer)
                

        #example tile add
        #pos is the x,y position. do it the same way as the player start position
        #image is the image used obviously
        #Get it by: image = ImageProc.getEntry(tiledict, index)
        # where index is a number from walls.txt. We should add constants to make this easier.
        #props is the object properties
        #props = tiledict[image]
        #layer is important for getting z indexing right.
        #layer = Area.getLayer(pos,size,props)
        #You need to make an area object based on the above information. Enemies can add afterwards for now
        areaMap = Area.Area([area,start,size,connections,tiledict,''])
        enemy = Sprites.buildArcher()
        enemy.rect.topleft = start
        areaMap.setActors([player,enemy])
        
        pickup = Item.buildSwordPickup()
        pickup2 = Item.buildGrapplingHookPickup()
        pickup3 = Item.buildBowPickup()
        pickup4 = Item.buildShieldPickup()
        pickup.rect.topleft = areaMap.getTilePosition([3,3])
        pickup2.rect.topleft = areaMap.getTilePosition([1,1])
        pickup3.rect.topleft = areaMap.getTilePosition([2,1])
        pickup4.rect.topleft = areaMap.getTilePosition([3,1])
        areaMap.setItems([pickup,pickup2,pickup3,pickup4])
        return areaMap
                       
