import pygame
import Area
import Sprites
import Character
import Display
import Input
import ImageProc
import Actions
#import MapThread
import threading
#import Queue
import UI
import Magic
from level import AgentLevelGenerator
from level import SimpleLevelGenerator
from item import Item
import PlayerModel
import argparse
import sys
from MouseAction import *
import Logger
import RollingLogger
import item.Utility as Utility

class GameEngine:
    time = 0
    PLAYING=0
    START=1
    def __init__(self,level):
        
        pos = [250,250]
        size = [20,50]
        Display.init([800,600])
        #ui = UI.UI(ImageProc.getUI())
        #Display.setUI(ui)
        Logger.init()
        RollingLogger.init()

        self.mouseState = MOUSE_MOVE
        self.mouseFunction = None
        hero = ImageProc.getSprite("hero",[3,4])
        
        sphereglow = ImageProc.getAnimation("sphereglow")
        explodelight = ImageProc.getAnimation("explodelight")
        self.player = Sprites.Person(hero["images"],Character.Character(),hero["properties"],[
                [pygame.K_UP,[Actions.move,[[0,-1]]]],
                [pygame.K_LEFT,[Actions.move,[[-1,0]]]],
                [pygame.K_DOWN,[Actions.move,[[0,1]]]],
                [pygame.K_RIGHT,[Actions.move,[[1,0]]]],
                 [pygame.K_z,[Actions.useItem,[]]],
                [pygame.K_a,[Actions.pickupItem,[]]],
                [pygame.K_x,[Actions.useSideItem,[]]]])
        self.player.character.speed = 20
        swords = ['Slash_left','Slash_up','Slash_left','Slash_up']
        #item = MeleeWeapon.Sword(1,1,swords)
        #item = Spell.FrostSpell(1,1,'frost')
        #item = Item.buildBow()
        #item = Utility.GrapplingHook()
        self.player.item = Item.buildSword(1, 1)
        Logger.addItemCount(self.player.item.type)
        self.player.character.health = 10
        levelCall = level + '()'
        levelGenerator = eval(levelCall)
        self.currentLevel = 1
        self.level = level
        self.playerModel = PlayerModel.initialPlayerModel()
        
        self.area = levelGenerator.createLevel(self.player,self.playerModel,self.currentLevel)
        
        Display.setArea(self.area)
        self.target = self.player
        Display.setPlayer(self.target)
        
        self.startTime = pygame.time.get_ticks()
        self.start = self.player.rect.topleft
    def startScreen(self):
        Input.checkInput()
        self.keys = Input.getKeys()
        
        self.mouse = Input.getMouseState(Display.getOffset(),True)
        
        nextMap = None
        if self.keys[pygame.K_RETURN]:
            self.__init__(self.level)
            self.currentState = self.PLAYING
            
        Display.showStart()
        
        #Display.wait(20)
        return Input.quitPressed()
    def loop(self):
        Input.checkInput()
        self.keys = Input.getKeys()
        
        self.mouse = Input.getMouseState(Display.getOffset(),True)
        for actor in self.area.actors:
            actor.decide(self.area,pygame.time.get_ticks(), self.keys,list(self.mouse))
            
        nextMap = None
        if self.keys[pygame.K_SPACE] and self.area.canTransfer():
            agent = eval(self.level+'()')
            
            currentTime = pygame.time.get_ticks()
            elapsedTime = currentTime-self.startTime
            Logger.setElapsedTime(elapsedTime)
            
            goal = self.player.rect.topleft
            Logger.distanceToGoal(self.start,goal)
            RollingLogger.addLevelStats(Logger.instance)

            self.playerModel = RollingLogger.adaptModelOverall(PlayerModel.PlayerModel(Logger.instance))

            self.startTime = currentTime
            #Logger.display()
            Logger.init()
            #self.playerModel.display()

            
            Logger.addItemCount(self.player.item.type)
            if not (self.player.sideItem == None):
                Logger.addItemCount(self.player.sideItem.type)
            self.currentLevel +=1
            self.area = agent.createLevel(self.player,self.playerModel,self.currentLevel)
            self.start = self.player.rect.topleft
            Display.setArea(self.area)
        
        Display.redraw(self.currentLevel)
        
        if self.player.character.health <= 0:
            self.currentState = self.START

        #Display.wait(20)
        return Input.quitPressed()
            
    def stop(self):
        #self.map.stop()
        Display.stop()
        

    def run(self):
        done =False
        self.currentState = self.START
        while(done == False):
            if self.currentState == self.PLAYING:
                cont = self.loop()
            else:
                cont = self.startScreen()
            done= cont

        self.stop()
    
def run(level):
    game = GameEngine(level)
    game.run()
if __name__ == "__main__":
    if "profile" in sys.argv:
        import hotshot
        import hotshot.stats
        import tempfile
        import os
 
        profile_data_fname = tempfile.mktemp("prf")
        try:
            prof = hotshot.Profile(profile_data_fname)
            prof.run('run(\'AgentLevelGenerator.AgentLevelGenerator\')')
            
            del prof
            
            s = hotshot.stats.load(profile_data_fname)
            s.strip_dirs()
            print "cumulative\n\n"
            s.sort_stats('cumulative').print_stats()
            print "By time.\n\n"
            s.sort_stats('time').print_stats()
            del s
        finally:
            # clean up the temporary file name.
            try:
                os.remove(profile_data_fname)
            except:
                # may have trouble deleting ;)
                pass
    else:
        parser = argparse.ArgumentParser(description='Run Dungeon')
        parser.add_argument('--level',action="store",default='AgentLevelGenerator', metavar='l',
		           help='Choose Level Generator. The default is the digger.')
        args = parser.parse_args()
        level = args.level
        level = level + '.' + level
        
        run(level)
