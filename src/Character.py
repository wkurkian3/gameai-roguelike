class Character:
    def __init__(self):
        self.health = 1
        self.attack = 1
        self.dexterity = 1
        self.constitution = 1
        self.intelligence = 1
        self.will = 1
        self.charisma = 1
        self.speed = 1
        self.combat = 1
        self.magic = 1
        self.gender = "male"
        self.spells = []
        self.defense = 0
    def clone(self):
        return self
